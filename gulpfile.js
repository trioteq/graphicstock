'use strict';
var gulp = require('gulp');
var sass = require('gulp-sass')(require('sass'));

//Sass
gulp.task('sass', async function () { 
    gulp.src('app/scss/**/*.scss')
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(gulp.dest('dist/css/'));
});

//watching task which checks for any file changes
gulp.task('watch', function(){
    gulp.watch('app/scss/*.scss', gulp.series('sass'));
});